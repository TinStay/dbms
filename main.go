package main

import (
	"fmt"
	"os"

	"bitbucket.org/TinStay/dbms/dbmsop"
)

func main() {
	// Create collection
	tasks, err := dbmsop.NewCollection("Tasks")

	if err != nil {
		fmt.Printf("Error in NewCollection method: %v \n", err)
		os.Exit(1)
	}

	// ADD RECORD
	id, err := tasks.AddRecord(map[string]interface{}{})

	fmt.Printf("Added record id: %v Err: %v \n", id, err)

	// GET RECORD
	record, err := tasks.GetOneRecord("Plan week", []string{})

	fmt.Printf("Action: GET RECORD; Received: %v ; Err: %v \n", record, err)

	// GET ALL RECORDS
	// records, err := tasks.GetAllRecords(true, []string{"_Completed"})
	// fmt.Println("Records:", records)

	// for _, record := range records {
	// 	fmt.Printf("Record: %v \n", record)
	// }

	// UPDATE RECORD
	// newRecord := map[string]interface{}{
	// 	"_Name":      "UPDATED",
	// 	"_StartedOn": "UPDATED",
	// 	"_Completed": false,
	// 	"_Subtasks":  1,
	// }
	// err = tasks.UpdateRecord("a422a93e-bd53-4099-b668-495a9d9b3bb4", newRecord)

	// fmt.Printf("Action: UPDATE RECORD; Success: %v;  Err: %v \n", err == nil, err)

	// DELETE RECORD
	// err = tasks.DeleteRecord("a422a93e-bd53-4099-b668-495a9d9b3bb4")

	// fmt.Printf("Action: DETELE RECORD; Success: %v ; Err: %v \n", err == nil, err)

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// OLD CODE //
	////////////////////////////////////////////////////////////////////////////////////////////////////

	// Get data from ini file
	// tasksData := dbms.GetTasksData()

	// for key, v := range tasksData.Indexes{
	// 	fmt.Printf("%v : %v \n", key, v)
	// }

	// ADD RECORD
	// id, _ := tasksData.AddRecord(`{"_Name": "Do laundry"}`)
	// fmt.Printf("Added record ID: %v \n", id)

	// FIND RECORD
	// _, err := tasksData.GetRecord("03/03/2022")
	// if err != nil {
	// 	fmt.Printf("Error in GetRecord method: %v \n", err)
	// 	os.Exit(1)
	// }

	// fmt.Printf("Record: %v \n", record)

	// UPDATE RECORD
	// err := tasksData.UpdateRecord("eb2650de-dddc-4b06-8959-172efba2fead",
	// 	`{"_Name":"Do laundry now","_ID":"ebeveve2650de-dddc-4b06-8959-172efba2fead"}`)

	// if err != nil {
	// 	fmt.Printf("Error in UpdateRecord method: %v \n", err)
	// 	os.Exit(1)
	// }

	// DELETE RECORD
	// err := tasksData.DeleteRecord("fda6a141-d96b-473f-bdbe-996652044337")
	// fmt.Printf("Error: %v \n", err)

}
