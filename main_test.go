package main

import (
	"encoding/json"
	"fmt"
	"testing"

	"bitbucket.org/TinStay/dbms/dbmsop"
	"gopkg.in/ini.v1"
)

var testCasesADD = []map[string]interface{}{
	{"_Completed": true, "_Name": "Complete CRUD methods", "_StartedOn": "12/21/12", "_Subtasks": 2, "_ID": "741a9217-0966-4fe6-a09b-97bb129a3876"},
	{"_Completed": false, "_Name": "Create a weekly plan", "_StartedOn": "04/21/12", "_Subtasks": 0, "_ID": "7f9c34a0-9be9-4657-ac19-1d09b7f6438b"},
	{"_Completed": false, "_Name": "Wash shoes", "_StartedOn": "06/1/22", "_Subtasks": 0, "_ID": "6c73a7c5-1076-4733-bbbf-e1b221df31d9"},
	{"_Name": "Plan weekly budget"},
	{},
}

var testCasesGETOne = map[interface{}][]string{
	"Complete CRUD methods": {"_Name", "_StartedOn", "_Completed"},
	false:                   {"_Completed"},
	2:                       {},
	0:                       {"_Subtasks"},
	"Plan weekly budget":    {"_Name", "_StartedOn", "_Completed"},
}
var testCasesGETAll = map[interface{}][]string{
	false: {"_Completed"},
	true:  {},
	2:     {},
	0:     {"_Subtasks"},
}

var testCasesUPDATE = map[string]map[string]interface{}{
	"741a9217-0966-4fe6-a09b-97bb129a3876": {"_Completed": true, "_Name": "Complete CRUD methods UPDATED", "_StartedOn": "12/21/12", "_Subtasks": 111, "_ID": "741a9217-0966-4fe6-a09b-97bb129a3876"},
	"7f9c34a0-9be9-4657-ac19-1d09b7f6438b": {"_Completed": true, "_Name": "Create a weekly plan UPDATED", "_StartedOn": "04/21/12", "_Subtasks": 222, "_ID": "7f9c34a0-9be9-4657-ac19-1d09b7f6438b"},
	"6c73a7c5-1076-4733-bbbf-e1b221df31d9": {"_Completed": true, "_Name": "Wash shoes UPDATED", "_StartedOn": "06/1/22", "_Subtasks": 333, "_ID": "6c73a7c5-1076-4733-bbbf-e1b221df31d9"},
}

var testCasesDELETE = []string{
	"741a9217-0966-4fe6-a09b-97bb129a3876",
	"7f9c34a0-9be9-4657-ac19-1d09b7f6438b",
	"6c73a7c5-1076-4733-bbbf-e1b221df31d9",
}

func TestAddRecord(t *testing.T) {
	// Data collection to test
	var tasksData, _ = dbmsop.NewCollection("TasksTest")

	// Remove all previous data records that existed in the file
	tasksData.ResetDataInCollection()

	for _, tc := range testCasesADD {
		// t.Run expects a string parameter
		stringTestCase := fmt.Sprintf("%v", tc)

		t.Run((stringTestCase), func(t *testing.T) {
			// ADD RECORD
			recordID, err := tasksData.AddRecord(tc)

			if err != nil {
				// Detect invalid test cases
				t.Errorf("error in adding record: %v \n", err)
			}

			// Update INI collection
			collection, err := ini.Load(tasksData.URL)

			if err != nil {
				t.Errorf("failed to load ini collection: %v \n", err)
			}

			// Check if record ID and record data is found in main dataset
			if !collection.Section("").HasKey(recordID) || collection.Section("").Key(recordID).String() == "" {
				// Record is not found
				t.Errorf("record with id=%v has not been added to dataset \n", recordID)
			}
		})
	}
}

func TestGetOneRecord(t *testing.T) {
	// Data collection to test
	var tasksData, _ = dbmsop.NewCollection("TasksTest")

	// Add records to collection
	// Use test function for adding records
	TestAddRecord(t)

	// Test Get method on added records
	for searchQuery, searchFilters := range testCasesGETOne {
		// t.Run expects a string parameter
		stringTestCase := fmt.Sprintf("%v", searchQuery)
		t.Run((stringTestCase), func(t *testing.T) {
			// GET RECORD
			record, err := tasksData.GetOneRecord(searchQuery, searchFilters)

			if err != nil {
				// TODO: Should wrong filters be tested
				// if searchQuery == 0 && len(searchFilters) == 1 && searchFilters[0] != "_Subtasks" {
				// 	fmt.Printf("Value '%v' is not found using the filters: %v \n", searchQuery, searchFilters)

				// } else {
				t.Errorf("error in getting one record: %v; Search query: %v; Search filters; %v \n", err, searchQuery, searchFilters)
				// }
			}

			if record == nil {
				// Record data is empty
				t.Errorf("record data is not found; Record: %v;  \n", record)
			}
		})
	}
}
func TestGetAllRecords(t *testing.T) {
	// Data collection to test
	var tasksData, _ = dbmsop.NewCollection("TasksTest")

	// Add records to collection
	// Use test function for adding records
	TestAddRecord(t)

	// Test Get method on added records
	for searchQuery, searchFilters := range testCasesGETAll {
		// t.Run expects a string parameter
		stringTestCase := fmt.Sprintf("%v", searchQuery)
		t.Run((stringTestCase), func(t *testing.T) {
			// GET RECORD
			records, err := tasksData.GetAllRecords(searchQuery, searchFilters)

			if err != nil {
				t.Errorf("error in getting all records: %v; Search query: %v; Search filters: %v \n", err, searchQuery, searchFilters)
			}
			fmt.Println("records", records)
			if records == nil {
				// Record data is empty
				t.Errorf("records data is not found; Records: %v; \n", records)
			}
		})
	}
}

func TestUpdateRecord(t *testing.T) {
	// Data collection to test
	var tasksData, _ = dbmsop.NewCollection("TasksTest")

	// Add records to collection
	// Use test fucntion for adding records
	TestAddRecord(t)

	// Test Update method on added records
	for id, tc := range testCasesUPDATE {
		t.Run((id), func(t *testing.T) {
			// UPDATE RECORD
			err := tasksData.UpdateRecord(id, tc)
			if err != nil {
				t.Errorf("Action: UPDATE RECORD; Success: %v;  Err: %v \n", err == nil, err)
			}

			// Manually check if records are updated
			// Check if parameter id is found inside record string
			// Check if new record string key-value pairs are found inside record string

			collection, _ := ini.Load(tasksData.URL)

			if err != nil {
				t.Errorf("failed to load ini collection: %v \n", err)
			}

			// Check if record ID exist in the main dataset
			if !collection.Section("").HasKey(id) {
				t.Errorf("record ID is not found in main dataset: %v \n", err)
			}

			// Check if values in ini file match the ones from passed parameters
			var currentRecord map[string]interface{}

			err = json.Unmarshal([]byte(collection.Section("").Key(id).String()), &currentRecord)

			if err != nil {
				t.Errorf("failed to unmarshal record: %v", err)
			}

			// tc - new record to update
			// currentRecord - record after update
			for key, value := range currentRecord {
				for k, v := range tc {
					if key == k {
						// Compare only string values to avoid type differences
						// 0 - could be int, could be float64
						if fmt.Sprintf("%v", value) != fmt.Sprintf("%v", v) {
							t.Errorf("Value of updated record doesn't match parameter; Received: %v; Expected: %v;", value, v)
						}
					}
				}
			}

		})
	}
}

func TestDeleteRecord(t *testing.T) {
	// Data collection to test
	var tasksData, _ = dbmsop.NewCollection("TasksTest")

	// Add records to collection
	// Use test fucntion for adding records
	TestAddRecord(t)

	// Test Update method on added records
	for _, tc := range testCasesDELETE {
		t.Run((tc), func(t *testing.T) {
			// DELETE RECORD
			err := tasksData.DeleteRecord(tc)
			if err != nil {
				t.Errorf("Action: DELETE RECORD; Success: %v;  Err: %v \n", err == nil, err)
			}

			// Manually check if records are deleted
			collection, err := ini.Load(tasksData.URL)

			if err != nil {
				t.Errorf("failed to load ini collection: %v \n", err)
			}

			// Check if deleted record ID exists in the main dataset - it MUST NOT
			if collection.Section("").HasKey(tc) {
				t.Errorf("deleted record id still exists in dataset \n")
			}
		})
	}
}
