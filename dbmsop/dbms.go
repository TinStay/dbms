package dbmsop

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"

	uuid "github.com/google/uuid"
	"gopkg.in/ini.v1"
)

var (
	ErrCollectionLoadFail        = errors.New("failed to load INI file")
	ErrCollectionNotExist        = errors.New("collection doesn't exist")
	ErrCollectionExists          = errors.New("collection already exists")
	ErrCollectionCreate          = errors.New("failed to create INI file")
	ErrCollectionLoadSection     = errors.New("failed to load INI section")
	ErrCollectionCreateID        = errors.New("failed to create ID for record")
	ErrCollectionDuplicateID     = errors.New("id is duplicate")
	ErrCollectionMarshalRecord   = errors.New("failed to marshal record")
	ErrCollectionUnmarshalRecord = errors.New("failed to unmarshal record")
	ErrRecordNotFound            = errors.New("record is not found")
	ErrRecordStringInvalidFormat = errors.New("new record string is invalid")
	ErrSearchStringInvalidFormat = errors.New("search string is invalid")
	ErrInvalidParametersFormat   = errors.New("parameter format is invalid")
)

// Helper functions
func IsValidUUID(u string) bool {
	_, err := uuid.Parse(u)
	return err == nil
}

func Contains(s []string, v string) bool {
	for _, a := range s {
		if a == v {
			return true
		}
	}
	return false
}

type Collection struct {
	Name string
	URL  string
}

func CustomError(subject, message error) error {
	return fmt.Errorf("%s | %s", subject.Error(), message.Error())
}

func NewCollection(name string) (Collection, error) {
	fileURL := fmt.Sprintf("DB/__%s.ini", name)

	// Create object
	collection := Collection{Name: name}
	collection.URL = fileURL

	// Check if collection exists
	if _, err := ini.Load(fileURL); err != nil {

		// Collection does not exist
		// Create file for collection
		if err = ioutil.WriteFile(fileURL, nil, 0777); err != nil {
			return collection, CustomError(ErrCollectionCreate, err)
		}

		return collection, nil
	}

	return collection, nil
}

// TODO: Should empty records be allowed => 7e34382d-6bb1-487f-8913-c16c7854d911 = {"_ID":"7e34382d-6bb1-487f-8913-c16c7854d911"}
// ADD RECORD
func (c *Collection) AddRecord(record map[string]interface{}) (string, error) {
	// Check if record has an ID
	if _, ok := record["_ID"]; !ok {
		// id is missing and needs to be added
		newId := uuid.New()
		if newId.String() == "" {
			return "", ErrCollectionCreateID
		}

		// Apply id to new record object
		record["_ID"] = newId.String()
	}

	// Load collection
	collection, err := ini.Load(c.URL)
	if err != nil {
		return "", ErrCollectionLoadFail
	}

	// Get record ID - string
	recordID := fmt.Sprintf("%v", record["_ID"])

	// Check if record is duplicate
	if collection.Section("").HasKey(recordID) {
		return "", ErrCollectionDuplicateID
	}

	var jsonBytes []byte

	// Convert record map into a JSON string
	jsonBytes, err = json.Marshal(record)
	if err != nil {
		return "", ErrCollectionMarshalRecord
	}

	// Add record to INI collection
	collection.Section("").Key(recordID).SetValue(string(jsonBytes))

	// Save INI collection
	collection.SaveTo(c.URL)

	// Return ID of added record
	return recordID, nil
}

// GET RECORD
func (c Collection) GetOneRecord(searchQuery interface{}, filters []string) (map[string]interface{}, error) {
	var record map[string]interface{}

	// Load collection
	collection, err := ini.Load(c.URL)
	if err != nil {
		return nil, ErrCollectionLoadFail
	}

	// Helper variables
	searchString := fmt.Sprintf("%v", searchQuery)
	var indexRef string
	var sections = filters

	if len(sections) == 0 {
		// Get all INI collection index sections except DEFAULT (main dataset)
		sections = collection.SectionStrings()[1:]
	}

	// Search all indexes
	for _, section := range sections {
		// Get INI section
		iniSection, err := collection.GetSection(section)

		if err != nil {
			continue
		}

		// Search for search query
		if iniSection.HasKey(searchString) {
			// Search query index is found
			indexRef = iniSection.Key(searchString).String()
		}
	}

	// Validate index reference
	if indexRef == "" {
		// Index does not exist
		// Check main dataset if search query is an ID
		if collection.Section("").HasKey(searchString) {
			// Search query is an ID and it is  found
			// Convert JSON string into a record map
			err = json.Unmarshal([]byte(collection.Section("").Key(searchString).String()), &record)

			if err != nil {
				return nil, CustomError(ErrCollectionUnmarshalRecord, err)
			}

			// Return record
			return record, nil
		}

		// Search query is not an ID
		// Loop through data
		for _, id := range collection.Section("").KeyStrings() {
			// Convert record to map in order to search the data in it
			var currentRecord map[string]interface{}
			err = json.Unmarshal([]byte(collection.Section("").Key(id).String()), &currentRecord)

			if err != nil {
				return nil, CustomError(ErrCollectionUnmarshalRecord, err)
			}

			// Check if search query is found
			for key, value := range currentRecord {
				stringValue := fmt.Sprintf("%v", value)

				if len(filters) == 0 {
					// Search through all data keys because there are no filters passed
					// Compare string values
					if stringValue == searchString {
						// Search value is found
						// Convert JSON string into a record map
						err = json.Unmarshal([]byte(collection.Section("").Key(id).String()), &record)

						if err != nil {
							return nil, CustomError(ErrCollectionUnmarshalRecord, err)
						}

						// Create and index for the searched query with the following structure:
						// [Key/Data field - e.g _Name]
						// searchString = [id of found record]

						var indexSection = fmt.Sprintf("%v", key)

						collection.Section(indexSection).Key(searchString).SetValue(id)

						// Save index to collection
						collection.SaveTo(c.URL)

						// Return record
						return record, nil
					}
				} else {
					// Search through filtered keys only
					if Contains(sections, key) {
						// Compare string values
						if stringValue == searchString {
							// Search value is found
							// Convert JSON string into a record map
							err = json.Unmarshal([]byte(collection.Section("").Key(id).String()), &record)

							if err != nil {
								return nil, CustomError(ErrCollectionUnmarshalRecord, err)
							}

							// Create and index for the searched query with the following structure:
							// [Key/Data field - e.g _Name]
							// searchString = [id of found record]

							var indexSection = fmt.Sprintf("%v", key)

							collection.Section(indexSection).Key(searchString).SetValue(id)

							// Save index to collection
							collection.SaveTo(c.URL)

							// Return record
							return record, nil
						}
					}
				}
			}
		}

		return record, ErrRecordNotFound

	}

	// Data index is found
	// Convert JSON string into a record map
	err = json.Unmarshal([]byte(collection.Section("").Key(indexRef).String()), &record)

	if err != nil {
		return nil, CustomError(ErrCollectionUnmarshalRecord, err)
	}

	return record, nil
}

// GET ALL RECORDS THAT MATCH A SEARCH QUERY
func (c Collection) GetAllRecords(searchQuery interface{}, filters []string) ([]map[string]interface{}, error) {
	var records []map[string]interface{}

	// Load collection
	collection, err := ini.Load(c.URL)
	if err != nil {
		return nil, ErrCollectionLoadFail
	}

	// Helper variables
	searchString := fmt.Sprintf("%v", searchQuery)

	// 	 TODO: Duplicate keys are not available in INI files
	// // Search all indexes and add records
	// if len(sections) == 0 {
	// 	// Get all INI collection index sections except DEFAULT (main dataset)
	// 	sections = collection.SectionStrings()[1:]
	// }

	// // Search all indexes
	// for _, section := range sections {
	// 	// Get INI section
	// 	iniSection, err := collection.GetSection(section)

	// 	if err != nil {
	// 		return emptyRecords, CustomError(ErrCollectionLoadSection, err)
	// 	}

	// 	for _, index := range iniSection.KeyStrings() {
	// 		if index == searchString {
	// 			recordIDs = append(recordIDs, iniSection.Key(searchString).String())
	// 		}
	// 	}
	// 	// Search for search query
	// 	// if iniSection.HasKey(searchString) {
	// 	// 	// Search query index is found
	// 	// 	recordIDs = append(recordIDs, iniSection.Key(searchString).String())
	// 	// }

	// }

	// Search all other data records
	for _, id := range collection.Section("").KeyStrings() {
		// Convert record to map in order to search the data in it
		var currentRecord map[string]interface{}
		err = json.Unmarshal([]byte(collection.Section("").Key(id).String()), &currentRecord)

		if err != nil {
			return nil, CustomError(ErrCollectionUnmarshalRecord, err)
		}

		for key, value := range currentRecord {
			stringValue := fmt.Sprintf("%v", value)

			if len(filters) == 0 {
				// Search through all data keys because there are no filters passed
				// Compare string values
				if stringValue == searchString {
					// Search value is found
					// Add record ID to list
					records = append(records, currentRecord)
				}
			} else {
				// Search through filtered keys only
				if Contains(filters, key) {
					// Compare string values
					if stringValue == searchString {
						// Search value is found
						// Add record ID to list
						records = append(records, currentRecord)
					}
				}
			}
		}
	}

	return records, nil
}

// random

// GET ALL RECORDS
func (c Collection) GetRecords() ([]map[string]interface{}, error) {
	var records []map[string]interface{}

	// Load collection
	collection, err := ini.Load(c.URL)
	if err != nil {
		return nil, CustomError(ErrCollectionLoadFail, err)
	}

	// Add all records to a slice
	for _, id := range collection.Section("").KeyStrings() {
		var currentRecord map[string]interface{}
		err = json.Unmarshal([]byte(collection.Section("").Key(id).String()), &currentRecord)
		if err != nil {
			return nil, CustomError(ErrCollectionUnmarshalRecord, err)
		}
		records = append(records, currentRecord)
	}

	// Return all records
	return records, nil
}

// TODO: Should index data change when record is updated
// UPDATE RECORD
func (c Collection) UpdateRecord(id string, newRecord map[string]interface{}) error {
	// Load collection
	collection, err := ini.Load(c.URL)
	if err != nil {
		return ErrCollectionLoadFail
	}

	// Check if records exists
	if collection.Section("").HasKey(id) {
		// Match record IDs - target record I parameter <=> new record ID
		if newRecord["_ID"] != id {
			newRecord["_ID"] = id
		}

		// Convert Go map into bytes
		var jsonBytes []byte

		jsonBytes, err = json.Marshal(newRecord)
		if err != nil {
			return ErrCollectionMarshalRecord
		}

		// Update record
		collection.Section("").Key(id).SetValue(string(jsonBytes))

		// Save INI collection
		collection.SaveTo(c.URL)

		return nil
	}

	// Records ID has not been found in collection
	return ErrRecordNotFound
}

// DELETE RECORD
func (c Collection) DeleteRecord(id string) error {
	// Validate id string parameter
	if id == "" || id == "{}" {
		return ErrInvalidParametersFormat
	}

	// Load collection
	collection, err := ini.Load(c.URL)
	if err != nil {
		return ErrCollectionLoadFail
	}

	// Check if record ID exists in collection
	if collection.Section("").HasKey(id) {
		// Delete record
		collection.Section("").DeleteKey(id)

		// Save INI collection
		collection.SaveTo(c.URL)

		return nil
	}

	return ErrRecordNotFound

}

func (c Collection) ResetDataInCollection() {
	emptyCollection := ini.Empty()

	// Set empty collection to INI
	emptyCollection.SaveTo(c.URL)
}

// func (c Collection) GetRecords() ([]string, error) {
// 	var records []string

// 	// Load collection
// 	collection, err :=ini.Load(c.URL)
// 	if err != nil {
// 		return nil, ErCollectionLoadFail
// 	}

// 	// Add all records to a slice
// 	for _, id := range collectionSection("").KeyStrings() {
// 		records = append(records, collection.Section("").Key(id.Value())
// 	}

// 	// Return all records
// 	return records, nil
// }
