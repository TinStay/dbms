module bitbucket.org/TinStay/dbms

go 1.18

require (
	github.com/google/uuid v1.3.0
	gopkg.in/ini.v1 v1.66.6
)

require github.com/stretchr/testify v1.7.5 // indirect
